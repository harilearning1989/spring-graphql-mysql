package com.web.demo.controls;

import com.web.demo.models.Employee;
import com.web.demo.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("emp")
public class EmployeeRestController {

    EmployeeService employeeService;

    @Autowired
    public EmployeeRestController setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
        return this;
    }

    @GetMapping("callOnlyOnce")
    public List<Employee> saveAllEmployees() {
        return employeeService.saveAll();
    }
}
