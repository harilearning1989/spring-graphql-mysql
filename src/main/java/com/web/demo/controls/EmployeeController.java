package com.web.demo.controls;

import com.web.demo.exceptions.EntityNotFoundException;
import com.web.demo.models.Employee;
import com.web.demo.records.EmployeeRecord;
import com.web.demo.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class EmployeeController {

    EmployeeService employeeService;

    @Autowired
    public EmployeeController setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
        return this;
    }

    @QueryMapping
    public List<Employee> findAll(){
        return employeeService.findAll();
    }

    @QueryMapping
    public List<EmployeeRecord> findEmpRecords(){
        return new ArrayList<>();
    }

    @QueryMapping
    public Employee findEmployeeById(@Argument int id){
        return employeeService.findById(id).get();
    }

    @MutationMapping
    public Employee createEmployee(@Argument Employee employee) {
        return employeeService.saveEmployee(employee);
    }

    @MutationMapping
    public boolean deleteEmployee(@Argument int id) {
        employeeService.deleteEmployee(id);
        return true;
    }

    @MutationMapping
    public Employee updateEmployeeById(
            @Argument(name = "empId") int id,
            @Argument(name = "username") String username,
            @Argument(name = "email") String email) throws EntityNotFoundException {
        Optional<Employee> optTutorial = employeeService.findById(id);

        if (optTutorial.isPresent()) {
            Employee employee = optTutorial.get();

            if (email != null)
                employee.setEmail(email);
            if (username != null)
                employee.setUsername(username);

            employeeService.save(employee);
            return employee;
        }

        throw new EntityNotFoundException("Not found Employee to update!");
    }

    @QueryMapping
    public long countEmployees() {
        return employeeService.countEmployees();
    }

}
