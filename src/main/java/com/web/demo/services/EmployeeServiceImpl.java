package com.web.demo.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.web.demo.models.Employee;
import com.web.demo.records.EmployeeRecord;
import com.web.demo.repos.EmployeeRepo;
import com.web.demo.utils.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    EmployeeRepo employeeRepo;
    JsonUtil jsonUtil;

    @Autowired
    public EmployeeServiceImpl setJsonUtil(JsonUtil jsonUtil) {
        this.jsonUtil = jsonUtil;
        return this;
    }

    @Autowired
    public EmployeeServiceImpl setEmployeeRepo(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
        return this;
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepo.findAll();
    }

    @Override
    public List<Employee> saveAll() {
        String employeeFile = "EmployeeData.json";
        List<EmployeeRecord> employeeRecords = jsonUtil.loadEntities(employeeFile, new TypeReference<List<EmployeeRecord>>() {});
        if (employeeRecords.isEmpty()) {
            System.out.println("No employees found or there was an error.");
        }
        List<Employee> employeeList = convertRecordToEntity(employeeRecords);
        employeeList = employeeRepo.saveAll(employeeList);
        return employeeList;
    }

    @Override
    public void deleteEmployee(int id) {
        employeeRepo.deleteById(id);
    }

    @Override
    public Optional<Employee> findById(int id) {
        return employeeRepo.findById(id);
    }

    @Override
    public void save(Employee employee) {
        employeeRepo.save(employee);
    }

    @Override
    public long countEmployees() {
        return employeeRepo.count();
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepo.save(employee);
    }

    private List<Employee> convertRecordToEntity(List<EmployeeRecord> employeeRecords) {
        return employeeRecords
                .stream()
                .map(m -> {
                    //Random rand = new Random();
                    //long randomNum = rand.nextInt((100000 - 10000) + 1) + 10000;

                    Employee.EmployeeBuilder employeeBuilder = Employee.builder()
                            .empName(m.name())
                            .username(m.username())
                            .age(m.age())
                            .email(m.email())
                            .gender(m.gender())
                            .phone(m.phone());

                    employeeBuilder.salary(m.salary());
                    employeeBuilder.version(m.version());
                    Employee employee = employeeBuilder.build();

                    return employee;
                })
                .toList();
    }
}
