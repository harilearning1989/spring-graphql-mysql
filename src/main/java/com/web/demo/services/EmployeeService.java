package com.web.demo.services;

import com.web.demo.models.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    List<Employee> findAll();
    List<Employee> saveAll();

    void deleteEmployee(int id);

    Optional<Employee> findById(int id);

    void save(Employee employee);

    long countEmployees();

    Employee saveEmployee(Employee employee);
}
