package com.web.demo.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.demo.models.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

@Component
public class JsonUtil {

    @Autowired
    private ResourceLoader resourceLoader;
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public List<Employee> readJsonFile(String fileName) {
        InputStream inputStream = null;
        try {
            inputStream = resourceLoader.getResource("classpath:" + fileName).getInputStream();
            return objectMapper.readValue(inputStream, new TypeReference<List<Employee>>() {});
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> List<T> loadEntities(String resourcePath, TypeReference<List<T>> typeReference) {
        Resource resource = resourceLoader.getResource("classpath:"+resourcePath);
        try {
            return objectMapper.readValue(resource.getInputStream(), typeReference);
        } catch (IOException e) {
            System.err.println("Error reading JSON file: " + e.getMessage());
        }
        return Collections.emptyList(); // Return an empty list if there's an error
    }
}
